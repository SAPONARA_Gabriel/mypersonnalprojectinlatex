all : Document_aide_informatique.pdf

Document_aide_informatique.pdf:Document_aide_informatique.tex
	pdflatex.exe -interaction=nonstopmode Document_aide_informatique.tex

clean:
	rm *.dvi
	rm *.log
	rm *.aux